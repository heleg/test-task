/* global locate */

Feature('Steps');

const locateStepPoint = index => locate('.step-point').at(index);

const passedBorderCss = {
  border: '12px solid rgb(81, 37, 219)',
};

const plainBorderCss = {
  border: '12px solid rgb(187, 187, 187)',
};

Scenario('Steps functional', async (I) => {
  I.amOnPage('/');
  I.see('First Caption', locateStepPoint(1));
  I.seeCssPropertiesOnElements(
    locateStepPoint(1).find('.circle'), passedBorderCss,
  );

  I.see('Second Caption', locateStepPoint(2));
  I.seeCssPropertiesOnElements(
    locateStepPoint(2).find('.circle'), plainBorderCss,
  );

  I.see('Third Caption', locateStepPoint(3));

  I.seeAttributesOnElements('.prev-step', { disabled: true });

  I.click('Next Step');
  I.seeCssPropertiesOnElements(
    locateStepPoint(2).find('.circle'), passedBorderCss,
  );

  I.click('Next Step');
  I.seeCssPropertiesOnElements(
    locateStepPoint(3).find('.circle'), passedBorderCss,
  );
  I.seeAttributesOnElements('.next-step', { disabled: true });

  I.click('Prev Step');
  I.seeCssPropertiesOnElements(
    locateStepPoint(3).find('.circle'), plainBorderCss,
  );

  I.fillField('.new-title', 'New Title');
  I.see('Add');
  I.click('Add');
  I.see('New Title');
  I.seeCssPropertiesOnElements(
    locateStepPoint(4).find('.circle'), plainBorderCss,
  );

  I.fillField('.new-title', 'Another Title');
  I.click('Add');
  I.see('Another Title');
  I.dontSee('Add');

  I.see('Remove');
  I.click('Remove');
  I.dontSee('First Caption');
  I.see('Add');

  I.click('Remove');
  I.dontSee('Second Caption');

  I.click('Remove');
  I.dontSee('Third Caption');

  I.dontSee('Remove');

  I.see('New Title');
  I.see('Another Title');
});
