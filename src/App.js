import React from 'react';
import PropTypes from 'prop-types';

import { compose, withStateHandlers } from 'recompose';

import Steps from './Steps';


const enhance = compose(
  withStateHandlers({
    currentStep: 0,
    titles: ['First Caption', 'Second Caption', 'Third Caption'],
    newTitleText: '',
  }, {
    nextStep: ({ currentStep }) => () => ({ currentStep: currentStep + 1 }),
    prevStep: ({ currentStep }) => () => ({ currentStep: currentStep - 1 }),
    addTitle: ({ titles, newTitleText }) => () => {
      if (!newTitleText) return {};

      return ({
        titles: [...titles, newTitleText],
        newTitleText: '',
      });
    },
    removeTitle: ({ titles, currentStep }) => (index) => {
      titles.splice(index, 1);
      return {
        titles,
        currentStep: currentStep >= index
          ? Math.max(0, currentStep - 1)
          : currentStep,
      };
    },
    setNewTitleText: () => text => ({ newTitleText: text }),
  }),
);

const AppBase = ({
  titles,
  currentStep,
  prevStep,
  nextStep,
  removeTitle,
  setNewTitleText,
  addTitle,
  newTitleText,
}) => (
  <div>
    <Steps currentStep={currentStep} titles={titles} />
    <button
      type="button"
      disabled={currentStep < 1}
      className="prev-step"
      onClick={prevStep}
    >
        Prev Step
    </button>
    <button
      type="button"
      onClick={nextStep}
      className="next-step"
      disabled={currentStep >= titles.length - 1}
    >
        Next Step
    </button>

    <ul>
      {titles.map((title, i) => (
        <li key={title}>
          {titles.length > 2 && (
          <button
            type="button"
            onClick={() => removeTitle(i)}
          >
Remove
          </button>
          )}
          {title}
        </li>
      ))}
    </ul>
    <input
      type="text"
      onChange={event => setNewTitleText(event.target.value)}
      className="new-title"
      value={newTitleText}
    />
    {titles.length < 5 && <button type="button" onClick={addTitle}>Add</button>}
  </div>
);

AppBase.propTypes = {
  titles: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  currentStep: PropTypes.number.isRequired,
  prevStep: PropTypes.func.isRequired,
  nextStep: PropTypes.func.isRequired,
  removeTitle: PropTypes.func.isRequired,
  setNewTitleText: PropTypes.func.isRequired,
  addTitle: PropTypes.func.isRequired,
  newTitleText: PropTypes.string.isRequired,
};

const App = enhance(AppBase);

export default App;
