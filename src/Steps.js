import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import StepPoint from './StepPoint';

const Wrapper = styled.div`
  display: flex;
  align-items: flex-end;
  margin: 0 100px;
`;

const Steps = ({ currentStep, titles }) => (
  <Wrapper>
    {titles.map((title, i) => (
      <StepPoint
        key={title}
        title={title}
        current={i === currentStep}
        passed={i < currentStep}
        last={i === titles.length - 1}
      />
    ))}
  </Wrapper>
);

Steps.propTypes = {
  currentStep: PropTypes.number.isRequired,
  titles: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

export default Steps;
