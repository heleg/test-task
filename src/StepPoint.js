import PropTypes from 'prop-types';
import React, { Fragment } from 'react';

import styled from 'styled-components';

const getColor = ({ passed, current }) => (
  passed || current
    ? '#5125db'
    : '#BBB'
);

const Point = styled.div`
  width: 50px;
`;

const Circle = styled.div`
  width: 50px;
  height: 50px;
  box-sizing: border-box;
  border: 12px solid ${getColor};
  border-radius: 50%;
  position: relative;
  z-index: 1;
`;

const Bar = styled.div`
  height: 12px;
  margin-bottom: 19px;
  background: #BBB;
  flex-grow: 1;
  margin-left: -2px;
  margin-right: -2px;
`;

const BarFiller = styled.div`
  width: ${props => (props.passed ? '100%' : '0')};
  height: 100%;
  background: #5125db;
  transition: width .3s;
`;

const CaptionContainer = styled.div`
  position: relative;
  right: -50%;
  width: 200px;
`;

const Caption = styled.div`
  font: 24px Arial;
  padding: 10px 0;
  box-sizing: border-box;
  text-align: center;
  font-weight: bold;
  color: ${getColor};
  left: -50%;
  position:relative;
`;

const StepPoint = ({
  title, last, passed, current,
}) => (
  <Fragment>
    <Point className="step-point">
      <CaptionContainer>
        <Caption passed={passed} current={current}>{title}</Caption>
      </CaptionContainer>
      <Circle className="circle" passed={passed} current={current} />
    </Point>
    {!last && (
    <Bar className="step-bar">
      <BarFiller passed={passed} />
    </Bar>
    )}
  </Fragment>
);

StepPoint.propTypes = {
  title: PropTypes.string.isRequired,
  last: PropTypes.bool.isRequired,
  passed: PropTypes.bool.isRequired,
  current: PropTypes.bool.isRequired,
};

export default StepPoint;
