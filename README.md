### Installation

```bash
yarn
```

### Launching

```bash
yarn start                      # development
yarn build && yarn start:server # production
```

### Testing
*chrome must be installed*
```bash
yarn test
```
